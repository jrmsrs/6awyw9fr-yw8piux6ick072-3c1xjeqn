import { writable } from 'svelte/store'

export const loading$ = writable(false)
export const authVisible$ = writable(false)
